import strutils, strtabs, xmltree

type
  Doc* = object
    brief*, deprecated*, details*, note*, remark*, ret*, sa*, warning*: string
    params*: seq[string]
    function*, functionName*, normalizedName*: string

proc looksLikeFunction*(f: string): bool =
  const types = [
    "cassowary",
    "effect",
    "event",
    "float",
    "int",
    "itemproperty",
    "json",
    "location",
    "object",
    "sqlquery",
    "string",
    "talent",
    "vector",
    "void",
  ]
  for prefix in types:
    if f.startsWith(prefix): return true
  f.startsWith("struct") and "(" in f

proc find*(n: XmlNode, tag: string, id = "", class = "", text = ""): XmlNode =
  if n == nil or n.kind != xnElement: return nil
  if n.len == 0: return nil
  # check all direct children first
  for idx in 0 ..< n.len:
    let el = n[idx]
    if el.kind != xnElement: continue
    if el.tag == tag:
      if id == "" and class == "" and text == "": return el
      if el.attrs == nil: continue
      let idMatch = id == "" or (el.attrs.hasKey("id") and el.attrs["id"] == id)
      let classMatch = class == "" or (el.attrs.hasKey("class") and class in el.attrs["class"].split(' '))
      let textMatch = text == "" or (el.len == 1 and el[0].kind in {xnText, xnComment, xnCData, xnEntity} and el[0].text == text)
      if idMatch and classMatch and textMatch:
        return el
  # then check childrens children
  for idx in 0 ..< n.len:
    let el = n[idx]
    if el.kind == xnElement and el.len > 0:
      result = el.find(tag, id, class, text)
      if result != nil: return result
  return nil
