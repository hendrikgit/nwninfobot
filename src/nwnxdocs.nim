import algorithm, json, os, strutils
from shared import Doc, looksLikeFunction

type
  State = enum
    nodoc
    function
    brief
    deprecated
    details
    note
    param
    remark
    ret
    sa
    warning

proc setContent (field: var string, l: string, idxKwEnd: int) =
  # set the field string to the line content unless there is nothing after the @keyword
  if l.len > idxKwEnd: field = l[idxKwEnd + 1 .. ^1]

proc getDocs (filename: string): seq[Doc] =
  var doc = Doc()
  var state = nodoc
  for l in filename.lines:
    if l.startsWith("/// "):
      var idxKwEnd = l.find(sub = ' ', start = 4)
      if idxKwEnd == -1: idxKwEnd = l.high + 1
      let kw = l[4 ..< idxKwEnd]
      case kw:
      of "@brief":
        state = brief
        doc.brief.setContent(l, idxKwEnd)
      of "@deprecated":
        state = deprecated
        doc.deprecated.setContent(l, idxKwEnd)
      of "@details":
        state = details
        doc.details.setContent(l, idxKwEnd)
      of "@note":
        state = note
        doc.note.setContent(l, idxKwEnd)
      of "@param":
        state = param
        if l.len > idxKwEnd: doc.params &= l[idxKwEnd + 1 .. ^1]
      of "@remark":
        state = remark
        doc.remark.setContent(l, idxKwEnd)
      of "@return":
        state = ret
        doc.ret.setContent(l, idxKwEnd)
      of "@sa": # sa: see also
        state = sa
        doc.sa.setContent(l, idxKwEnd)
      of "@warning":
        state = warning
        doc.warning.setContent(l, idxKwEnd)
      elif not l.startsWith("/// @"): # ignore other @tags that are not considererd here yet
        # the current line will be added to the existing text with a leading space
        # or a line break depending on the field
        case state:
        of nodoc, function: continue
        of brief: doc.brief &= l[3 .. ^1]
        of deprecated: doc.deprecated &= l[3 .. ^1]
        of details: doc.details &= l[3 .. ^1]
        of note:
          if doc.note != "": doc.note &= "\n"
          doc.note &= l[4 .. ^1]
        of param: doc.params[^1] &= l[3 .. ^1]
        of remark: doc.remark &= l[3 .. ^1]
        of ret:
          if doc.ret != "": doc.ret &= "\n"
          doc.ret &= l[4 .. ^1]
        of sa: doc.sa &= l[3 .. ^1]
        of warning: doc.warning &= l[3 .. ^1]
    elif state != nodoc:
      if state == function or l.looksLikeFunction:
        # if current line is "{" it means there was no forward declaration and the function body is about to begin
        if not l.endsWith(';') and l != "{":
          # function is split over more than one line
          state = function
          if doc.function != "": doc.function &= " "
          doc.function &= l.strip()
        else:
          # function is complete (or now complete with the current line)
          state = nodoc
          if l != "{":
            if doc.function != "": doc.function &= " "
            doc.function &= l.strip()
          let f = doc.function
          let idxFuncStart = f.find(' ', start = if f.startsWith("struct"): 7 else: 0)
          doc.functionName = f[idxFuncStart + 1 ..< f.find('(')]
          doc.normalizedName = doc.functionName.normalize
          result &= doc
          doc = Doc()
      else:
        state = nodoc

proc removeRef(s: var string) =
  if "@ref " notin s: return
  let idxRef = s.find("@ref ")
  s = s[0 ..< idxRef] & s[s.find(' ', start = idxRef + 5) + 1 .. ^1]

proc replaceNwnWithLinks(s: var string) =
  # @nwn {SetBaseAttackBonus}
  if "@nwn{" notin s: return
  let iStart = s.find("@nwn{")
  let iEnd = s.find('}', start = iStart + 5)
  if iEnd == -1: return
  let content = s[iStart + 5 ..< iEnd]
  let link = "[" & content & "](https://nwnlexicon.com/index.php?title=" & content & ")"
  s = s[0 ..< iStart] & link & s[iEnd + 1 .. ^1]

proc postProcess(s: var string) =
  var prev = ""
  while s != prev:
    prev = s
    s.removeRef
  prev = ""
  while s != prev:
    prev = s
    s.replaceNwnWithLinks

when isMainModule:
  if paramCount() != 1:
    echo "1 parameter required: path to nwnxee plugins"
    quit(QuitFailure)

  var docs = newSeq[Doc]()
  for f in paramStr(1).walkDirRec():
    if not f.endsWith(".nss") or f.endsWith("_t.nss"): continue
    echo f
    var nssDocs = f.getDocs
    for doc in nssDocs.mitems:
      # filter out @ref
      doc.brief.postProcess
      doc.deprecated.postProcess
      doc.details.postProcess
      doc.note.postProcess
      doc.remark.postProcess
      doc.ret.postProcess
      doc.sa.postProcess
      doc.warning.postProcess
      for p in doc.params.mitems: p.postProcess
      docs &= doc

  docs.sort proc (a, b: auto): int = a.normalizedName.cmp(b.normalizedName)

  writeFile("nwnx.json", (%docs).pretty)
