import asyncdispatch, httpClient, htmlparser, os, strtabs, strutils, xmltree
from shared import find

const
  baseURL = "https://nwnlexicon.com"
  functionsPath = "/index.php?title=Category:Functions"
  htmlDir = "lexicon"

proc saveUrl(url, filename: string) {.async.} =
  let future = newAsyncHttpClient().getContent(url)
  yield future
  if future.failed:
    echo "Error getting: ", url, ", ", future.readError.name
  else:
    echo "Writing file: ", filename
    writeFile(filename, future.read)

type
  URL = tuple
    url, name: string

var functionLinks = newSeq[URL]()
proc getFunctionLinks(url: string) {.async.} =
  echo url
  let response = await newAsyncHttpClient().getContent(url)
  let pagesDiv = response.parseHtml.find("div", id = "mw-pages")
  let functionsDiv = pagesDiv.find("div", class = "mw-content-ltr").find("div", class = "mw-category")
  if functionsDiv != nil:
    let links = functionsDiv.findAll("a")
    if links.len > 0:
      echo "Function links found: ", links.len
      echo "First: ", links[0].attrs["title"]
      echo "Last: ", links[^1].attrs["title"]
      for link in links:
        functionLinks &= (baseURL & link.attrs["href"], link.attrs["title"])
  let nextPage = pagesDiv.find("a", text = "next page")
  if nextPage != nil:
    await getFunctionLinks(baseUrl & nextPage.attrs["href"])

waitFor getFunctionLinks(baseURL & functionsPath)
echo "Function links in total: ", functionLinks.len

createDir(htmlDir)
# limit number of requests at a time
const batch = 100
var startIdx = 0
while true:
  let endIdx = if startIdx + batch > functionLinks.len: functionLinks.len else: startIdx + batch
  echo startIdx ..< endIdx
  var futures = newSeq[Future[void]]()
  for link in functionLinks[startIdx ..< endIdx]:
    let filename = joinPath(htmlDir, link.name & ".html")
    if fileExists(filename):
      echo "File exists: ", filename
    else:
      futures &= saveUrl(link.url, filename)
  waitFor all futures
  if endIdx == functionLinks.len: break
  startIdx.inc batch
