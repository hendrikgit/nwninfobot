import asyncdispatch, colors, json, logging, options, os, sequtils, strutils, tables
import dimscord
from shared import Doc

const
  nwnxjsonFilename = "nwnx.json"
  lexiconjsonFilename = "lexicon.json"
  logFormat = "$date $time $levelname: "
  requiredPermissions = {permSendMessages, permEmbedLinks}
  lexiconURL = "https://nwnlexicon.com/index.php?title="

const helpText = """This bot helps with searching for and looking up of functions.
`!n searchterm` shows **n**wnx functions as indexed from the nwnx documentation comments
`!f searchterm` shows base game **f**unctions as indexed from <https://nwnlexicon.com/>
Search is case insensitive and underscore `_` will be ignored.
Add a `$` to the end of your search term if you want to only get results that end there.

Additional commands:
`!help` This help text.
`!ncount` Number of indexed nwnx functions.
`!fcount` Number of indexed base game functions.
`!invitelink` Show an invite link to add this bot to a server.
"""

addHandler(newConsoleLogger(fmtStr = logFormat))
addHandler(newRollingFileLogger(filename = "nwninfobot.log", mode = fmAppend, bufSize = 0, fmtStr = logFormat))

for file in [nwnxjsonFilename, lexiconjsonFilename]:
  if not fileExists(file):
    error file & " not found. Generate the file first."
    quit(QuitFailure)

if not existsEnv("TOKEN"):
  error "Please set environment variable TOKEN to the bots token."
  quit(QuitFailure)
let botToken = getEnv("TOKEN")

let docsNwnx = nwnxjsonFilename.parseFile.mapIt it.to Doc
let docsLexicon = lexiconjsonFilename.parseFile.mapIt it.to Doc

proc toEmbed (doc: Doc, link = ""): Embed
proc originInfo (m: Message, s: Shard): Future[string] {.async.}

let discord = newDiscordClient(botToken)

proc onReady (s: Shard, r: Ready) {.event(discord).} =
  info "Ready as " & $r.user
  info "Invite link: " & r.user.id.createBotInvite(requiredPermissions)
  await s.updateStatus(activity = some ActivityStatus(name: "DMs for searches", kind: atListening))

proc messageCreate(s: Shard, m: Message) {.event(discord).} =
  if m.author.bot: return
  if m.content == "!help":
    info (await originInfo(m, s)) & ": " & m.content
    discard await discord.api.sendMessage(m.channel_id, helpText)
  elif m.content == "!invitelink":
    info (await originInfo(m, s)) & ": !invitelink"
    discard await discord.api.sendMessage(m.channel_id, "Invite link for **" & s.user.username & "**: <" & s.user.id.createBotInvite(requiredPermissions) & ">")
  elif m.content in ["!ncount", "!fcount"]:
    info (await originInfo(m, s)) & ": " & m.content
    let msg = if m.content[1] == 'n':
      "Number of indexed nwnx functions: " & $docsNwnx.len
    else:
      "Number of indexed base game functions: " & $docsLexicon.len
    discard await discord.api.sendMessage(m.channel_id, msg)
  elif m.content.startsWith("!n ") or m.content.startsWith("!f ") and m.content.len > 3:
    let docs = if m.content[1] == 'n': docsNwnx else: docsLexicon
    let searchTerm = m.content[3 .. ^1].replace(" ").normalize
    let results =
      if searchTerm.endsWith('$'):
        docs.filterIt it.normalizedName.endsWith(searchTerm[0 .. ^2])
      else:
        docs.filterIt searchTerm in it.normalizedName
    info (await originInfo(m, s)) & ": !" & m.content[1] & " Search for \"" & m.content[3 .. ^1] & "\" (" & searchTerm & "): " & $results.len & " results"
    case results.len:
    of 0:
      discard await discord.api.sendMessage(m.channel_id, "Nothing found")
    of 1:
      let r = results[0]
      discard await discord.api.sendMessage(
        m.channel_id,
        embeds = @[r.toEmbed(link = if m.content[1] == 'f': lexiconURL & r.functionName else: "")]
      )
    of 2 .. 30:
      var content = "Results found: " & $results.len & "\n"
      for r in results:
        content &= r.functionName & "\n"
      discard await discord.api.sendMessage(m.channel_id, content)
    else:
      discard await discord.api.sendMessage(m.channel_id, "Too many results found: " & $results.len)

waitFor discord.startSession(gateway_intents = {giGuilds, giGuildMessages, giDirectMessages, giMessageContent})

proc indentFunc(function: string): string =
  if "()" in function: return function
  result = function.replace("(", "(\n" & 7.spaces)
  return result.replace(", ", ",\n" & 7.spaces)

proc add(fields: var seq[EmbedField], name, value: string, inline = false) =
  if value.len == 0 or name.len == 0: return
  fields &= EmbedField(
    name: name,
    value: value,
    inline: if inline: some true else: none bool
  )

proc findParamSplit(s: string): int =
  for i, c in s:
    if c == ' ':
      if i == 0 or s[i - 1] != ',':
        return i
  return -1

proc escape(s: string): string =
  result = s
  for c in [r"\", "_", "*", "~", "`", "|"]:
    result = result.replace(c, r"\" & c)

proc toEmbed(doc: Doc, link = ""): Embed =
  if doc.deprecated != "" or doc.warning != "":
    result.color = some colRed.int
  result.title = some doc.functionName
  var description = ""
  if link != "": description &= "<" & link & ">\n"
  description &= "```c\n" & doc.function.indentFunc & "\n```\n" & doc.brief.escape
  if doc.deprecated != "": description &= "\n\n***Deprecated! " & doc.deprecated.escape & "***"
  if doc.warning != "": description &= "\n\n**Warning!** " & doc.warning.escape
  if description.len > 2048: # Discord character limit for description
    description = description[0 .. 2044] & "..."
  result.description = some description
  var fields = newSeq[EmbedField]()
  if doc.params.len > 0:
    for p in doc.params:
      let i = p.findParamSplit
      if i > 0:
        fields.add p[0 ..< i], p[i + 1 .. ^1], inline = true
  fields.add "Returns", doc.ret
  fields.add "Details", doc.details
  fields.add "Note", doc.note
  fields.add "Remark", doc.remark
  fields.add "See also", doc.sa
  for field in fields.mitems:
    field.value = field.value.escape
  result.fields = some fields

proc getGuild(guild_id: string, s: Shard): Future[Guild] {.async.} =
  if guild_id in s.cache.guilds:
    return s.cache.guilds[guild_id]
  let guild = await discord.api.getGuild(guild_id)
  s.cache.guilds[guild_id] = guild
  return guild

proc originInfo(m: Message, s: Shard): Future[string] {.async.} =
  if m.guild_id.isSome:
    result = "[" & (await m.guild_id.get.getGuild(s)).name & "]"
  else: result = "[]"
  if m.channel_id in s.cache.guildChannels:
    result &= "[" & s.cache.guildChannels[m.channel_id].name & "]"
  else: result &= "[]"
  result &= "[" & $m.author & "]"
