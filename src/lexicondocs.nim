import algorithm, htmlparser, json, os, strtabs, strutils, xmltree
import regex
from shared import find, looksLikeFunction, Doc

const htmlDir = "lexicon"

var files = 0
var docs = newSeq[Doc]()

proc getIndex(n: XmlNode, tag: string, id = "", class = ""): int =
  for el in n:
    if el.kind == xnElement and el.tag == tag:
      if id == "" and class == "": return result
      if el.attrs != nil:
        let idMatch = id == "" or (el.attrs != nil and el.attrs.hasKey("id") and el.attrs["id"] == id)
        let classMatch = class == "" or (el.attrs != nil and el.attrs.hasKey("class") and class in el.attrs["class"])
        if idMatch and classMatch:
          return result
    result.inc
  return -1

proc delete(n: XmlNode, tag: string, id = "", class = ""): bool =
  let idx = n.getIndex(tag, id, class)
  if idx != -1:
    n.delete(idx)
    return true

for file in walkFiles(joinPath(htmlDir, "*.html")):
  files.inc
  let html = loadHtml(file)
  let functionNode = html.find("div", class = "nwscript")
  if functionNode != nil:
    let f = functionNode.innerText.strip
      .replace(re2"[\r\n]", "") # remove linebreaks first
      .replace(re2"\s", " ") # change nbsp and other kinds of whitespace to regular space
      .replace(re2"\s{2,}", " ") # reduce 2 or more spaces to one space
      .replace(",", ", ")
    if f.looksLikeFunction:
      let idxFuncStart = f.find(' ', start = if f.startsWith("struct"): 7 else: 0)
      if idxFuncStart != -1 and idxFuncStart < f.find('('):
        var doc = Doc(function: f)
        doc.functionName = f[idxFuncStart + 1 ..< f.find('(')]
        doc.normalizedName = doc.functionName.normalize
        let content = html.find("div", id = "mw-content-text").find("div", class = "mw-parser-output")
        if content != nil:
          discard content.delete("div", id = "toc")
          discard content.delete("h1")
          var deleted = true
          while deleted: # remove all code divs, examples can be too long for discord
            deleted = content.delete("div", class = "mw-geshi mw-content-ltr")
          doc.brief = content.innerText
            .replace(re2"[\n\r]{2,}", "\n\n")
            .replace("\u00a0", " ") # nbsp to space
            .replace(re2"(?m)^\s+author:.*$", "")
            .replace(re2"(?m)^\s+Example\s+$", "Example available in nwnlexicon")
            .strip
        docs &= doc
      else:
        echo "Error: Function name could not be determined:"
        echo file, "\n", functionNode, "\n", f
    else:
      echo "Error: Function not recognized:"
      echo file, "\n", functionNode, "\n", f
  else:
    echo "Error: pre tag not found:"
    echo file

echo "Files: ", files
echo "Functions recognized: ", docs.len
docs.sort proc (a, b: auto): int = a.normalizedName.cmp(b.normalizedName)
writeFile("lexicon.json", (%docs).pretty)
