# nwninfobot
Shows information about [nwnxee](https://github.com/nwnxee/unified/) functions and base game functions (from [NWN Lexicon](https://nwnlexicon.com/)).

The data is read from nwnxee nwscript source files and scraped from the NWN Lexicons webpage. This data is saved to JSON files. On startup the bot reads those json files and keeps the content in memory for quick search.

## Invite link
Use this link to invite the bot to your server https://discord.com/api/oauth2/authorize?client_id=733761578146463764&scope=bot&permissions=18432.

## Screenshot
![discord nwninfobot sceenshot](nwninfobot.png)
