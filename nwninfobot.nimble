# Package
version       = "0.1.0"
author        = "Hendrik Albers"
description   = "Discord bot to search and show nwnxee functions"
license       = "MIT"
srcDir        = "src"
bin           = @["nwninfobot", "nwnxdocs", "lexiconscrape", "lexicondocs"]

# Dependencies
requires "nim ^= 2.0.0"
requires "dimscord ^= 1.6.0"
requires "regex ^= 0.23.0"
